# xcluster wiki

## local build and preview

1. use mdbook from cargo
```
cargo install mdbook
mdbook build
mdbook serve -p 3001
# open browser to visit localhost:3001
```

2. use prebuilt mdbook
```
wget https://github.com/rust-lang/mdBook/releases/download/v0.4.36/mdbook-v0.4.36-x86_64-unknown-linux-gnu.tar.gz
tar xzvf mdbook*.tar.gz
./mdbook build
./mdbook serve -p 3001
# open browser to visit localhost:3001
```