# Summary

[xcwiki](./index.md)
- [Quick Start](./chap1_intro.md)
  - [LSF 基本用法](./chap1-1_lsf.md)
  - [简单理解 module](./chap1-2_module.md)
- [软件使用指南](./chap2.md)
  - [VASP](./chap2_vasp.md)
  - [PySCF](./chap2_pyscf.md)
  - [ORCA](./chap2_orca.md)
  - [Gaussian](./chap2_gaussian.md)
  - [GAMESS](./chap2_gamess.md)
- [编译工具](./chap3.md)
- [如何编辑此 wiki](./chap6.md)


