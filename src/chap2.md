# 软件使用指南

各软件的使用指南应包括

- 如何在集群上提交任务
- 提交任务时的注意事项
- 如何自己安装该软件（如有必要）
- 如何修改任务提交脚本（如有必要）

