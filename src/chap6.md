# 6 Developer's Guide
## 6.1 How to contribute


## 6.2 Tips for CI/CD and packaging

### How to write GitLab CI yml file


## 6.3 Write wiki

The current online wiki is enabled via [mdBook](https://github.com/rust-lang/mdBook), utilizing [Yethiel's template](https://gitlab.com/yethiel/pages-mdbook) and modified [Nord Theme](https://github.com/gbrlsnchs/mdBook-nord-template).

For configuration tips (rendering, themes, etc.), see [mdBook Doc](https://rust-lang.github.io/mdBook/format/configuration/index.html). 

All chapters should be registered in `SUMMARY.md`, and thus they will appear in the side bar.

### Build and preview
We have included the `mdbook` linux binary in the source code, so simply do

```bash
wget https://github.com/rust-lang/mdBook/releases/download/v0.4.36/mdbook-v0.4.36-x86_64-unknown-linux-gnu.tar.gz
tar xzvf mdbook*.tar.gz
# modify md in src
./mdbook build
./mdbook serve -p 3001
# open your browser to visit localhost:3001
```

On other platforms, you can download the binaries in the GitHub repo of mdBook and mdbook-toc, or use WSL (you don't need a browser inside WSL), or use `cargo install mdbook`.

Simple modifications can be done on the GitLab website. Clicking the `Edit` button or `Open the Web IDE` allows you modify and commit online.

### Markdown syntax

You should [learn some basic Markdown](https://rust-lang.github.io/mdBook/format/markdown.html) to modify the documentation.

However, mdBook's [math support](https://rust-lang.github.io/mdBook/format/mathjax.html) is a little different from common Markdown syntax. 
The inline math should be written as `\\( \\)`.

**Flowchart**

[Mermaid](https://mermaid.js.org/syntax/flowchart.html) is supported.

<!--
We now have a flowchart example at the beginning of [Section 4.5](./chap4-5.md), which is an [AntV G6](https://g6.antv.antgroup.com/manual/introduction) graph embedded in an iframe. The G6 graph can be configured in `chap4-5_head.html` and `data.json`.

But for simple flowcharts, like `A -> B -> C`, it's not necessary to use G6. Instead, [Mermaid](https://mermaid.js.org/syntax/flowchart.html) is suitable, and well supported by Markdown, although we don't have a chance to use it yet.
-->