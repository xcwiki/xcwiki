# xcluster wiki

wiki for xcluster.

Jan 15, 2024

<br>

**Contents**

- [Quick Start](./chap1_intro.md)
  - [LSF 基本用法](./chap1-1_lsf.md)
  - [简单理解 module](./chap1-2_module.md)
- [软件使用指南](./chap2.md)
  - [VASP](./chap2_vasp.md)
  - [PySCF](./chap2_pyscf.md)
  - [ORCA](./chap2_orca.md)
  - [Gaussian](./chap2_gaussian.md)
  - [GAMESS](./chap2_gamess.md)
- [编译工具](./chap3.md)
  - GCC
  - Intel
  - Rust
- [如何编辑此 wiki](./chap6.md)


**Tips**

You can use the &#128269; tool in the left upper corner of this page to search the whole wiki, or simply press `s` on your keyboard.

