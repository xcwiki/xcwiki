# LSF 基本用法

## 简单理解 bsub

若直接运行某程序的命令为 `python a.py`，那么将其提交到 LSF 的命令为
```
bsub "python a.py"
```
当然，一般来说我们需要指定队列和核数，如
```
bsub -q small -n 4 "python a.py"
```
可使用 [freenodes](#freenodes) 查看有哪些可用的队列。

## bsub 参数

- `-R`
- `-M`

## bsub 脚本

## 实用命令

### `freenodes` 

查看当前空闲的节点

### `bjobs`

运行`bjobs`可查看当前用户的（正在运行/排队的）作业情况。进一步地，可以
- `bjobs -q small` 仅查看某一队列的作业
- `bjobs -u somebody` 查看某用户的作业，`bjobs -u all` 查看所有用户的作业，常搭配`-q`使用
- `bjobs -l JobID` 查看某一作业的详细情况，如当前使用内存，CPU时间等。也适用于已结束的作业。`bjobs -l`将查看当前运行的所有作业。

### `bacct`

`bacct -l JobID` 查看某一已结束作业的详细情况。比`bjobs -l`更详细一点。

### `bhosts`

`bhosts -l NodeName` 查看某节点有多少CPU/内存。可通过`freenodes`查看有哪些 Node，如 xc08n01。